
import { Component, OnInit, Input } from '@angular/core';
import { SmoothieService, Smoothie } from '../smoothie.service';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-smoothie',
  templateUrl: './smoothie.component.html',
  styleUrls: ['./smoothie.component.css']
})
export class SmoothieComponent implements OnInit {

  private _smoothies: Smoothie[];
  private imgUrl = environment.apiUrl;
  constructor(
    private smoothieService: SmoothieService, 
    private router: Router
    ) { }

  ngOnInit() {
    this.getAllSmoothies();
  }

  set smoothies(mSmoothies: any) {
    this._smoothies = mSmoothies;
  }

  get smoothies(): any {
    return this._smoothies;
  }

  getAllSmoothies(): void {
    this.smoothieService.getSmoothies()
      .subscribe((listSmoothie) => {
        this.smoothies = listSmoothie;
        console.log(this.smoothies);
      });
  }

}


