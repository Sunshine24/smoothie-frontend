import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import { SmoothieService, Smoothie } from '../smoothie.service';

@Component({
  selector: 'app-smoothie-form',
  templateUrl: './smoothie-form.component.html',
  styleUrls: ['./smoothie-form.component.css']
})
export class SmoothieFormComponent implements OnInit {

  // On crée une propriété pour le smoothie en cours
  smoothie: Smoothie = {
    title: '',
    ingredients: [
      {
        nom: '',
        quantite: '',
      }
      ],
    features: {
      cost: '',
      prepareTime: '',
    },
    advice: '',
    description: '',
    steps: [
      {
        stepText: '',
      }
      ],
    photo: [
      {
        title: '',
        path: '',
        description: '',
      }
      ]
    };

  smoothieForm: FormGroup;           // variable smoothieForm

  constructor(
    private fb: FormBuilder,     // FormBuilder is a librarie
    private smoothieService: SmoothieService
  ) { }

  ngOnInit() {
    this.buildForm();
  }

  get myForm() {
    return this.smoothieForm.controls;
  }

  private buildForm() {                              // to build the form
    this.smoothieForm = this.fb.group({
      hideRequired: false,
      floatLabel: 'auto',
      title: ['', Validators.required],
    });
  }

  onSubmit() {
    // stop here if form is invalid
    if (this.smoothieForm.invalid) {
      return;
    } else {
      this.smoothie.title = this.myForm.title.value;
      this.addSmoothie(this.smoothie);
      // console.log(this.smoothie);
    }
  }

  addSmoothie(smoothie: Smoothie) {
    this.smoothieService.addSmoothie(smoothie)
      .subscribe( (lastInsertSmoothie) => {
        console.log('smoothie inséré', lastInsertSmoothie);
      });
  }

}
