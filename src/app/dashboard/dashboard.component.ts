import { Component, OnInit, Input } from '@angular/core';
import { SmoothieService, Smoothie } from '../smoothie.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  private _smoothies: Smoothie[];

  constructor(private smoothieService: SmoothieService) {
  }

  ngOnInit() {
    this.getAllSmoothies();
  }

  set smoothies(mSmoothies: any) {
    this._smoothies = mSmoothies;
  }

  get smoothies(): any {
    return this._smoothies;
  }

  getAllSmoothies(): void {
    this.smoothieService.getSmoothies()
      .subscribe((listSmoothie) => {
        this.smoothies = listSmoothie;
        console.log(this.smoothies);
      });
  }

}


