import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule, } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { AppHeaderComponent } from './app-header/app-header.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { SmoothieComponent } from './smoothie/smoothie.component';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SmoothieService } from './smoothie.service';
import { DetailComponent } from './detail/detail.component';
import { MatTabsModule, MatGridListModule, MatFormFieldModule } from '@angular/material';
import { MatListModule } from '@angular/material/list';
import { SmoothieFormComponent } from './smoothie-form/smoothie-form.component';


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    AppHeaderComponent,
    SmoothieComponent,
    DetailComponent,
    SmoothieFormComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    MatInputModule,
    MatCardModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    HttpClientModule,
    RouterModule,
    BrowserAnimationsModule,
    MatTabsModule,
    MatListModule,
    MatGridListModule,
    ReactiveFormsModule,
    MatFormFieldModule,
  ],
  providers: [SmoothieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
