import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

export interface Smoothie {
  _id?: string;
  title: string;
  ingredients: [
    {
      nom: string;
      quantite: string;
    }
  ];
  features: {
    cost?: string;
    prepareTime: string;
  };
  advice?: string;
  description: string;
  steps: [
    {
      stepText: string;

    }

  ],
  photo?: [
    {
      title?: string;
      path?: string;
      description?: string;
    }
  ]
}

@Injectable({
  providedIn: 'root'
})

export class SmoothieService {

  private apiUrl = environment.apiUrl;

  constructor(private http: HttpClient, private router: Router) {
  }

  // Get all Smoothie by observable from backend
  getSmoothies(): Observable<Smoothie[]> {
    const smoothieListUrl = `${this.apiUrl}/catalog/list`;
    return this.http.get<Smoothie[]>(smoothieListUrl);
  }

  getOneSmoothie(id: String): Observable<Smoothie> {
    const getUrl = `${this.apiUrl}/catalog/${id}`;
    console.log(getUrl);
    return this.http.get<Smoothie>(getUrl);
  }

  addSmoothie(smoothie: Smoothie) {
    const addUrl = `${this.apiUrl}/catalog/add`;
    return this.http.post<Smoothie>(addUrl, smoothie);
  }

}
